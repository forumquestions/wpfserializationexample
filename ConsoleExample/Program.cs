﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SerializationExample.Common;

namespace ConsoleExample
{
    partial class Program
    {
        #region Entry point

        private static Program _program;

        static Task Main(string[] args)
        {
            _program = new Program();
            return _program.Run(args);
        }

        #endregion
    }

    partial class Program
    {
        private async Task Run(string[] args)
        {
            List<QuoteInfo> result;
            try
            {
                result = await BasicCallAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadKey();
                return;
            }

            foreach (QuoteInfo quote in result)
            {
                Display(quote);
            }

            Console.ReadKey();
        }

        private async Task<List<QuoteInfo>> BasicCallAsync()
        {
            using (var client = new HttpClient())
            {
                //var content = await client.GetStringAsync("url");

                #region Пример
                // Если ненужно, то можно удалить весь регион с содержимым
                await Task.CompletedTask;
                string content = ResourceProvider.Instance.ReadJsonExample();

                #endregion

                var quoteInfos = JsonConvert.DeserializeObject<List<QuoteInfo>>(content);
                return quoteInfos;
            }
        }

        private void Display(QuoteInfo quotes)
        {
            Console.WriteLine($"Id: {quotes.Id}, Text: {quotes.Price}");
        }
    }
}

﻿using Newtonsoft.Json;
using SerializationExample.Common.Json.Converters;

namespace ConsoleExample
{
    public class QuoteInfo
    {
        [JsonProperty("id", Required = Required.Always)]
        public int Id { get; set; }

        [JsonConverter(typeof(DecimalConverter))]
        [JsonProperty("price", Required = Required.Always)]
        public decimal Price { get; set; }

        // Если это деньги, то типы нужно сменить на decimal, double для этого не подходит.
        [JsonConverter(typeof(DoubleConverter))] 
        [JsonProperty("qty", Required = Required.Always)]
        public double Qty { get; set; } // Не знаю, что такое Qty, но, похоже, не Quantity

        // Если это деньги, то типы нужно сменить на decimal, double для этого не подходит.
        [JsonConverter(typeof(DoubleConverter))]
        [JsonProperty("quoteQty", Required = Required.Always)]
        public double QuoteQty { get; set; }

        [JsonProperty("time", Required = Required.Always)]
        public long Time { get; set; }

        [JsonProperty("isBuyerMaker", Required = Required.Always)]
        public bool IsBuyerMaker { get; set; }

        [JsonProperty("isBestMatch", Required = Required.Always)]
        public bool IsBestMatch { get; set; }
    }
}
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using SerializationExample.Common;
using SerializationExample.Common.Model;
using SerializationExample.Mvvm.Converters;
using SerializationExample.Mvvm.Messaging;

namespace SerializationExample.ViewModels
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        public ObservableCollection<QuoteInfoViewModel> Data { get; }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            Data = new ObservableCollection<QuoteInfoViewModel>();
            MessengerInstance.Register<NotificationMessage<Command>>(this, CommandReceived);
        }

        private void CommandReceived(NotificationMessage<Command> message)
        {
            switch (message.Content)
            {
                case Command.Init:
                    Init();
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Init()
        {
            string content = ResourceProvider.Instance.ReadJsonExample();
            var quoteInfos = JsonConvert.DeserializeObject<List<QuoteInfo>>(content);

            foreach (QuoteInfo info in quoteInfos)
            {
                var viewModel = new QuoteInfoViewModel(info);
                Data.Add(viewModel);

                if (viewModel.Price > 499m)
                {
                    viewModel.Status = QuoteStatus.Overprice;
                }
            }
        }
    }
}
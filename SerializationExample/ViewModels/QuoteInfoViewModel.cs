﻿using GalaSoft.MvvmLight;
using SerializationExample.Common.Model;
using SerializationExample.Mvvm.Converters;

namespace SerializationExample.ViewModels
{
    public class QuoteInfoViewModel : ViewModelBase
    {
        private readonly QuoteInfo _info;
        private QuoteStatus _status;

        public QuoteStatus Status
        {
            get { return _status; }
            set { Set(ref _status, value); }
        }

        public int Id
        {
            get { return _info.Id; }
        }

        public decimal Price
        {
            get { return _info.Price; }
        }

        public bool IsBestMatch
        {
            get { return _info.IsBestMatch; }
        }

        public QuoteInfoViewModel(QuoteInfo info)
        {
            _info = info;
        }
    }
}

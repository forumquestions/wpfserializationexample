﻿using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using SerializationExample.Mvvm.Messaging;

namespace SerializationExample.Views
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            // Текстовое сообщение отправляется просто для примера. 
            // Вполне нормальный способ обмена сообщениями между UI и подписчиками, хотя можно было
            // бы получить из ресурсов ViewModelLocator и обратиться к свойству Main, вызвать нужный метод.
            Messenger.Default.Send(new NotificationMessage<Command>(Command.Init, ""));
        }
    }
}

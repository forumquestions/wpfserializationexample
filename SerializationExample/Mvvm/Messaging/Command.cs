﻿namespace SerializationExample.Mvvm.Messaging
{
    /// <summary>
    /// Команды, которые можно посылать, например, из UI.
    /// </summary>
    public enum Command
    {
        Init
    }
}

﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SerializationExample.Mvvm.Converters
{
    [ValueConversion(typeof(QuoteStatus), typeof(bool))]
    public class StatusOverpriceToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((QuoteStatus)value) == QuoteStatus.Overprice;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}

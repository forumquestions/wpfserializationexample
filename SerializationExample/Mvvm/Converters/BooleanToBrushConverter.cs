﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace SerializationExample.Mvvm.Converters
{
    [ValueConversion(typeof(bool), typeof(SolidColorBrush))]
    public class BooleanToBrushConverter : IValueConverter
    {
        public SolidColorBrush StatusOverpriceBrush { get; set; } = new SolidColorBrush(Colors.Tomato);

        public SolidColorBrush StatusDefaultBrush { get; set; } = new SolidColorBrush(Colors.Black);

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? StatusOverpriceBrush : StatusDefaultBrush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace SerializationExample.Common
{
    public class ResourceProvider
    {
        public static ResourceProvider Instance { get; }

        static ResourceProvider()
        {
            Instance = new ResourceProvider();
        }

        private ResourceProvider()
        {
        }

        public string ReadJsonExample()
        {
            /* Смотри как ресурс добавляется в SerializationExample.Common.csproj
  <ItemGroup>
    <None Remove="Data.json" />
  </ItemGroup>

  <ItemGroup>
    <EmbeddedResource Include="Data.json" />
  </ItemGroup>
             */
            return ReadManifestData("Data.json");
        }

        public static string ReadManifestData(string embeddedFileName)
        {
            return ReadManifestData<ResourceProvider>(embeddedFileName);
        }

        private static string ReadManifestData<TSource>(string embeddedFileName) 
            where TSource : class
        {
            var assembly = typeof(TSource).GetTypeInfo().Assembly;
            var resourceName = assembly.GetManifestResourceNames()
                .First(s => s.EndsWith(embeddedFileName, StringComparison.CurrentCultureIgnoreCase));

            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                if (stream == null)
                {
                    throw new InvalidOperationException("Could not load manifest resource stream.");
                }
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}
